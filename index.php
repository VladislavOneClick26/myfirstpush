<?php

date_default_timezone_set("Europe/Moscow");
$timepoint = strtotime("06.04.2020 00:00");

$curtime = date('d.m.Y G:i:s');
$curtime1 = strtotime($curtime);

$ostatok = $timepoint - $curtime1;
$hour = $ostatok/3600;
$hour1 = floor($hour);
$minute = $ostatok%3600;
$minute1 = floor($minute/60);

$is_auth = (bool) rand(0, 1);
$title = 'Заголовок страницы';
$user_name = 'Константин';
$user_avatar = 'img/user.jpg';

$categories = ["Доски и лыжи","Крепления","Ботинки","Одежда","Инструменты","Разное"];

$lots_list = [
	[
		'title' => "2014 Rossignol District Snowboard",
		'category' => $categories[0],
		'price' => 10999,
		'img' => 'img/lot-1.jpg'
	],
	[
		'title' => "DC Ply Mens 2016/2017 Snowboard",
		'category' => $categories[0],
		'price' => 159999,
		'img' => 'img/lot-2.jpg'
	],
	[
		'title' => "Крепления Union Contact Pro 2015 года размер L/XL",
		'category' => $categories[1],
		'price' => 8000,
		'img' => 'img/lot-3.jpg'
	],
	[
		'title' => "Ботинки для сноуборда DC Mutiny Charocal",
		'category' => $categories[2],
		'price' => 10999,
		'img' => 'img/lot-4.jpg'
	],
	[
		'title' => "Куртка для сноуборда DC Mutiny Charocal",
		'category' => $categories[3],
		'price' => 7500,
		'img' => 'img/lot-5.jpg'
	],
	[
		'title' => "Маска Uakley Canopy",
		'category' => $categories[5],
		'price' => 5400,
		'img' => 'img/lot-6.jpg'
	]
];

function formatprice($price) {
	$afterformatprice = number_format($price, 0, '', ' ');

	return $afterformatprice;
};

require_once 'functions.php';

$layout_content = temp_gen('index.php',[
	'timeostH'=>$hour1,
	'timeostM'=>$minute1,
	'categories'=>$categories,
	'lots_list'=>$lots_list
]);

$layout = temp_gen('layout.php',[
	'title'=>$title,
	'is_auth'=>$is_auth,
	'user_name'=>$user_name,
	'user_avatar'=>$user_avatar,
	'categories'=>$categories,
	'layout_content'=>$layout_content
]);

print($layout);

?>

