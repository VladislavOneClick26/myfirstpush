<?php

    $header = template('header', ['title' => 'Hello World!']);
    $content = template('content', ['content' => 'Lorem ipsum...', 'meta' => 'Author info']);
    $footer = template('footer', ['copy' => 'Copyright ' . date('Y')]);

echo $header, $content, $footer;

/**
 * @param  string $template
 * @param  array $vars
 * @return string
 */
function template($template, $vars) {
    ob_start();
    extract($vars);
    require_once "templates/$template.php";
    return ob_get_clean();
}

