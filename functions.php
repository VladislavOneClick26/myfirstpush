<?php

function esc($str) {
	$text = strip_tags($str);

	return $text;
}

function temp_gen($path, $temp_data) {
    ob_start();
    extract($temp_data);
	if(isset($path)){
        $content = require_once ('templates/'.$path);
    }
    return ob_get_clean();
}